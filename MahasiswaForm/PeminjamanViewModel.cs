﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MahasiswaForm
{
    class PeminjamanViewModel
    {
        public int id { set; get; }
        public string user { set; get; }
        public string buku { set; get; }
        public DateTime? peminjaman { set; get; }
        public DateTime? pengembalian { set; get; }
        public DateTime? pengembalianUser { set; get; }
        public double? Denda { set; get; }
    }
}
