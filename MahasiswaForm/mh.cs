//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MahasiswaForm
{
    using System;
    using System.Collections.Generic;
    
    public partial class mh
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mh()
        {
            this.PeminjamanBukus = new HashSet<PeminjamanBuku>();
        }
    
        public string IdMhs { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Jurusan { get; set; }
        public string JenisKelamin { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PeminjamanBuku> PeminjamanBukus { get; set; }
    }
}
