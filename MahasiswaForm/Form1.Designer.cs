﻿namespace MahasiswaForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mName = new System.Windows.Forms.TextBox();
            this.mId = new System.Windows.Forms.TextBox();
            this.mAlamat = new System.Windows.Forms.TextBox();
            this.mJurusan = new System.Windows.Forms.TextBox();
            this.mJenisKelamin = new System.Windows.Forms.TextBox();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.mGridView = new System.Windows.Forms.DataGridView();
            this.dataTab = new System.Windows.Forms.TabControl();
            this.mhs = new System.Windows.Forms.TabPage();
            this.book = new System.Windows.Forms.TabPage();
            this.peminjaman = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bName = new System.Windows.Forms.TextBox();
            this.bPengarang = new System.Windows.Forms.TextBox();
            this.db_mahasiswaDataSet = new MahasiswaForm.db_mahasiswaDataSet();
            this.bukuBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bukuTableAdapter = new MahasiswaForm.db_mahasiswaDataSetTableAdapters.BukuTableAdapter();
            this.bSimpan = new System.Windows.Forms.Button();
            this.bUpdate = new System.Windows.Forms.Button();
            this.bDelete = new System.Windows.Forms.Button();
            this.bGridView = new System.Windows.Forms.DataGridView();
            this.textviewgan = new System.Windows.Forms.Label();
            this.bId = new System.Windows.Forms.TextBox();
            this.pIdBook = new System.Windows.Forms.ComboBox();
            this.pIdUser = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.PGridView = new System.Windows.Forms.DataGridView();
            this.pPinjam = new System.Windows.Forms.Button();
            this.pKembalikan = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mGridView)).BeginInit();
            this.dataTab.SuspendLayout();
            this.mhs.SuspendLayout();
            this.book.SuspendLayout();
            this.peminjaman.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.db_mahasiswaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bukuBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Mahasiswa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nama";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Alamat";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Jurusan";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Jenis Kelamin";
            // 
            // mName
            // 
            this.mName.Location = new System.Drawing.Point(135, 64);
            this.mName.Name = "mName";
            this.mName.Size = new System.Drawing.Size(266, 22);
            this.mName.TabIndex = 5;
            // 
            // mId
            // 
            this.mId.Location = new System.Drawing.Point(135, 19);
            this.mId.Name = "mId";
            this.mId.Size = new System.Drawing.Size(266, 22);
            this.mId.TabIndex = 6;
            // 
            // mAlamat
            // 
            this.mAlamat.Location = new System.Drawing.Point(135, 107);
            this.mAlamat.Name = "mAlamat";
            this.mAlamat.Size = new System.Drawing.Size(266, 22);
            this.mAlamat.TabIndex = 7;
            // 
            // mJurusan
            // 
            this.mJurusan.Location = new System.Drawing.Point(135, 152);
            this.mJurusan.Name = "mJurusan";
            this.mJurusan.Size = new System.Drawing.Size(266, 22);
            this.mJurusan.TabIndex = 8;
            // 
            // mJenisKelamin
            // 
            this.mJenisKelamin.Location = new System.Drawing.Point(135, 199);
            this.mJenisKelamin.Name = "mJenisKelamin";
            this.mJenisKelamin.Size = new System.Drawing.Size(266, 22);
            this.mJenisKelamin.TabIndex = 9;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Location = new System.Drawing.Point(16, 534);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(75, 31);
            this.btnSimpan.TabIndex = 11;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = true;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(98, 533);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 32);
            this.btnUpdate.TabIndex = 12;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnHapus
            // 
            this.btnHapus.Location = new System.Drawing.Point(179, 534);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(75, 31);
            this.btnHapus.TabIndex = 13;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = true;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // mGridView
            // 
            this.mGridView.AllowUserToOrderColumns = true;
            this.mGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mGridView.Location = new System.Drawing.Point(16, 236);
            this.mGridView.Name = "mGridView";
            this.mGridView.RowTemplate.Height = 24;
            this.mGridView.Size = new System.Drawing.Size(755, 282);
            this.mGridView.TabIndex = 14;
            // 
            // dataTab
            // 
            this.dataTab.Controls.Add(this.mhs);
            this.dataTab.Controls.Add(this.book);
            this.dataTab.Controls.Add(this.peminjaman);
            this.dataTab.Location = new System.Drawing.Point(13, 12);
            this.dataTab.Name = "dataTab";
            this.dataTab.SelectedIndex = 0;
            this.dataTab.Size = new System.Drawing.Size(785, 613);
            this.dataTab.TabIndex = 15;
            // 
            // mhs
            // 
            this.mhs.Controls.Add(this.mGridView);
            this.mhs.Controls.Add(this.btnHapus);
            this.mhs.Controls.Add(this.btnUpdate);
            this.mhs.Controls.Add(this.btnSimpan);
            this.mhs.Controls.Add(this.mJenisKelamin);
            this.mhs.Controls.Add(this.mJurusan);
            this.mhs.Controls.Add(this.mAlamat);
            this.mhs.Controls.Add(this.mId);
            this.mhs.Controls.Add(this.mName);
            this.mhs.Controls.Add(this.label5);
            this.mhs.Controls.Add(this.label4);
            this.mhs.Controls.Add(this.label3);
            this.mhs.Controls.Add(this.label2);
            this.mhs.Controls.Add(this.label1);
            this.mhs.Location = new System.Drawing.Point(4, 25);
            this.mhs.Name = "mhs";
            this.mhs.Padding = new System.Windows.Forms.Padding(3);
            this.mhs.Size = new System.Drawing.Size(777, 584);
            this.mhs.TabIndex = 0;
            this.mhs.Text = "Mahasiswa";
            this.mhs.UseVisualStyleBackColor = true;
            // 
            // book
            // 
            this.book.Controls.Add(this.bId);
            this.book.Controls.Add(this.textviewgan);
            this.book.Controls.Add(this.bGridView);
            this.book.Controls.Add(this.bDelete);
            this.book.Controls.Add(this.bUpdate);
            this.book.Controls.Add(this.bSimpan);
            this.book.Controls.Add(this.bPengarang);
            this.book.Controls.Add(this.bName);
            this.book.Controls.Add(this.label7);
            this.book.Controls.Add(this.label6);
            this.book.Location = new System.Drawing.Point(4, 25);
            this.book.Name = "book";
            this.book.Padding = new System.Windows.Forms.Padding(3);
            this.book.Size = new System.Drawing.Size(777, 584);
            this.book.TabIndex = 1;
            this.book.Text = "Buku";
            this.book.UseVisualStyleBackColor = true;
            // 
            // peminjaman
            // 
            this.peminjaman.Controls.Add(this.pKembalikan);
            this.peminjaman.Controls.Add(this.pPinjam);
            this.peminjaman.Controls.Add(this.PGridView);
            this.peminjaman.Controls.Add(this.label9);
            this.peminjaman.Controls.Add(this.label8);
            this.peminjaman.Controls.Add(this.pIdUser);
            this.peminjaman.Controls.Add(this.pIdBook);
            this.peminjaman.Location = new System.Drawing.Point(4, 25);
            this.peminjaman.Name = "peminjaman";
            this.peminjaman.Size = new System.Drawing.Size(777, 584);
            this.peminjaman.TabIndex = 2;
            this.peminjaman.Text = "Peminjaman";
            this.peminjaman.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Nama Buku";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(33, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Pengarang";
            // 
            // bName
            // 
            this.bName.Location = new System.Drawing.Point(157, 67);
            this.bName.Name = "bName";
            this.bName.Size = new System.Drawing.Size(277, 22);
            this.bName.TabIndex = 2;
            // 
            // bPengarang
            // 
            this.bPengarang.Location = new System.Drawing.Point(157, 118);
            this.bPengarang.Name = "bPengarang";
            this.bPengarang.Size = new System.Drawing.Size(277, 22);
            this.bPengarang.TabIndex = 3;
            // 
            // db_mahasiswaDataSet
            // 
            this.db_mahasiswaDataSet.DataSetName = "db_mahasiswaDataSet";
            this.db_mahasiswaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bukuBindingSource
            // 
            this.bukuBindingSource.DataMember = "Buku";
            this.bukuBindingSource.DataSource = this.db_mahasiswaDataSet;
            // 
            // bukuTableAdapter
            // 
            this.bukuTableAdapter.ClearBeforeFill = true;
            // 
            // bSimpan
            // 
            this.bSimpan.Location = new System.Drawing.Point(540, 189);
            this.bSimpan.Name = "bSimpan";
            this.bSimpan.Size = new System.Drawing.Size(134, 48);
            this.bSimpan.TabIndex = 5;
            this.bSimpan.Text = "Simpan";
            this.bSimpan.UseVisualStyleBackColor = true;
            this.bSimpan.Click += new System.EventHandler(this.bSimpan_Click);
            // 
            // bUpdate
            // 
            this.bUpdate.Location = new System.Drawing.Point(540, 243);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(134, 52);
            this.bUpdate.TabIndex = 6;
            this.bUpdate.Text = "Update";
            this.bUpdate.UseVisualStyleBackColor = true;
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // bDelete
            // 
            this.bDelete.Location = new System.Drawing.Point(540, 301);
            this.bDelete.Name = "bDelete";
            this.bDelete.Size = new System.Drawing.Size(134, 46);
            this.bDelete.TabIndex = 7;
            this.bDelete.Text = "Hapus";
            this.bDelete.UseVisualStyleBackColor = true;
            this.bDelete.Click += new System.EventHandler(this.bDelete_Click);
            // 
            // bGridView
            // 
            this.bGridView.AllowUserToAddRows = false;
            this.bGridView.AllowUserToDeleteRows = false;
            this.bGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bGridView.Location = new System.Drawing.Point(7, 191);
            this.bGridView.Name = "bGridView";
            this.bGridView.ReadOnly = true;
            this.bGridView.RowTemplate.Height = 24;
            this.bGridView.Size = new System.Drawing.Size(527, 387);
            this.bGridView.TabIndex = 8;
            // 
            // textviewgan
            // 
            this.textviewgan.AutoSize = true;
            this.textviewgan.Location = new System.Drawing.Point(36, 28);
            this.textviewgan.Name = "textviewgan";
            this.textviewgan.Size = new System.Drawing.Size(19, 17);
            this.textviewgan.TabIndex = 9;
            this.textviewgan.Text = "Id";
            // 
            // bId
            // 
            this.bId.Location = new System.Drawing.Point(157, 28);
            this.bId.Name = "bId";
            this.bId.Size = new System.Drawing.Size(277, 22);
            this.bId.TabIndex = 10;
            // 
            // pIdBook
            // 
            this.pIdBook.FormattingEnabled = true;
            this.pIdBook.Location = new System.Drawing.Point(28, 64);
            this.pIdBook.Name = "pIdBook";
            this.pIdBook.Size = new System.Drawing.Size(465, 24);
            this.pIdBook.TabIndex = 0;
            // 
            // pIdUser
            // 
            this.pIdUser.FormattingEnabled = true;
            this.pIdUser.Location = new System.Drawing.Point(28, 143);
            this.pIdUser.Name = "pIdUser";
            this.pIdUser.Size = new System.Drawing.Size(465, 24);
            this.pIdUser.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "LIST BUKU";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 17);
            this.label9.TabIndex = 3;
            this.label9.Text = "LIST MAHASISWA";
            // 
            // PGridView
            // 
            this.PGridView.AllowUserToAddRows = false;
            this.PGridView.AllowUserToDeleteRows = false;
            this.PGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PGridView.Location = new System.Drawing.Point(31, 202);
            this.PGridView.Name = "PGridView";
            this.PGridView.ReadOnly = true;
            this.PGridView.RowTemplate.Height = 24;
            this.PGridView.Size = new System.Drawing.Size(591, 320);
            this.PGridView.TabIndex = 4;
            // 
            // pPinjam
            // 
            this.pPinjam.Location = new System.Drawing.Point(548, 123);
            this.pPinjam.Name = "pPinjam";
            this.pPinjam.Size = new System.Drawing.Size(75, 43);
            this.pPinjam.TabIndex = 5;
            this.pPinjam.Text = "Pinjam";
            this.pPinjam.UseVisualStyleBackColor = true;
            this.pPinjam.Click += new System.EventHandler(this.pPinjam_Click);
            // 
            // pKembalikan
            // 
            this.pKembalikan.Location = new System.Drawing.Point(628, 202);
            this.pKembalikan.Name = "pKembalikan";
            this.pKembalikan.Size = new System.Drawing.Size(146, 43);
            this.pKembalikan.TabIndex = 6;
            this.pKembalikan.Text = "Kembalikan";
            this.pKembalikan.UseVisualStyleBackColor = true;
            this.pKembalikan.Click += new System.EventHandler(this.pKembalikan_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 637);
            this.Controls.Add(this.dataTab);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mGridView)).EndInit();
            this.dataTab.ResumeLayout(false);
            this.mhs.ResumeLayout(false);
            this.mhs.PerformLayout();
            this.book.ResumeLayout(false);
            this.book.PerformLayout();
            this.peminjaman.ResumeLayout(false);
            this.peminjaman.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.db_mahasiswaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bukuBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox mName;
        private System.Windows.Forms.TextBox mId;
        private System.Windows.Forms.TextBox mAlamat;
        private System.Windows.Forms.TextBox mJurusan;
        private System.Windows.Forms.TextBox mJenisKelamin;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.DataGridView mGridView;
        private System.Windows.Forms.TabControl dataTab;
        private System.Windows.Forms.TabPage mhs;
        private System.Windows.Forms.TabPage book;
        private System.Windows.Forms.TabPage peminjaman;
        private System.Windows.Forms.TextBox bPengarang;
        private System.Windows.Forms.TextBox bName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private db_mahasiswaDataSet db_mahasiswaDataSet;
        private System.Windows.Forms.BindingSource bukuBindingSource;
        private db_mahasiswaDataSetTableAdapters.BukuTableAdapter bukuTableAdapter;
        private System.Windows.Forms.Button bDelete;
        private System.Windows.Forms.Button bUpdate;
        private System.Windows.Forms.Button bSimpan;
        private System.Windows.Forms.DataGridView bGridView;
        private System.Windows.Forms.TextBox bId;
        private System.Windows.Forms.Label textviewgan;
        private System.Windows.Forms.ComboBox pIdUser;
        private System.Windows.Forms.ComboBox pIdBook;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView PGridView;
        private System.Windows.Forms.Button pPinjam;
        private System.Windows.Forms.Button pKembalikan;
    }
}

