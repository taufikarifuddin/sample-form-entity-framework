﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MahasiswaForm
{
    public partial class Form1 : Form
    {
        mhsEntities db = new mhsEntities();
        public Form1()
        {
            InitializeComponent();          
            DisplayData();
            DisplayDataBook();
            InitializePeminjaman();
            DisplayDataPeminjaman();
        }

        private void InitializePeminjaman()
        {
            pIdUser.DataSource = db.mhs.ToList();
            pIdUser.ValueMember = "IdMhs";
            pIdUser.DisplayMember = "Nama";

            pIdBook.DataSource = db.Bukus.ToList();
            pIdBook.ValueMember = "Id";
            pIdBook.DisplayMember = "Name";
        }

        private void DisplayData()
        {
            List<mh> listMhs = db.mhs.ToList();
            mGridView.DataSource = listMhs;
            InitializePeminjaman();
        }

        private void DisplayDataBook()
        {
            List<Buku> listBuku = db.Bukus.ToList();
            bGridView.DataSource = listBuku;
            InitializePeminjaman();
        }

        private void DisplayDataPeminjaman()
        {
            List<PeminjamanViewModel> listPeminjaman = db.PeminjamanBukus
                .Where( m => m.PengembalianUser == null )
                .Select(m => new PeminjamanViewModel {
                    buku = m.Buku.Name,
                    id = m.Id,
                    peminjaman = m.Pengambilan,
                    pengembalian = m.Pengembalian,
                    user = m.mh.Nama,
                    pengembalianUser = m.PengembalianUser,
                    Denda = m.Pengembalian < DateTime.Now ?
                            System.Data.Entity.DbFunctions.DiffDays(m.Pengembalian, DateTime.Now) * 3000 : null
                }).ToList();

            PGridView.DataSource = listPeminjaman;
        }

        enum UpdateStatus
        {
            EDIT,NEW_RECORD
        }

        public void resetField()
        {
            mId.Text = "";
            mName.Text = "";
            mAlamat.Text = "";
            mJurusan.Text = "";
            mJenisKelamin.Text = "";
        }

        public void resetBookField()
        {
            bId.Text = "";
            bName.Text = "";
            bPengarang.Text = "";
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            mh mhs = db.mhs.Where(m => m.IdMhs.Equals(mId.Text.ToString()))
                .FirstOrDefault();

            UpdateStatus status = 
                UpdateStatus.EDIT;
            if (mhs == null)
            {
                mhs = new mh();
                status = UpdateStatus.NEW_RECORD;
            }
            mhs.IdMhs = mId.Text.ToString();
            mhs.Nama = mName.Text.ToString();
            mhs.Alamat = mAlamat.Text.ToString();
            mhs.Jurusan = mJurusan.Text.ToString();
            mhs.JenisKelamin = mJenisKelamin.Text.ToString();

            try
            {

                if (status == UpdateStatus.NEW_RECORD)
                {
                    db.mhs.Add(mhs);
                }
                else
                {
                    db.Entry(mhs).State = System.Data.Entity.EntityState.Modified;
                }
                
                db.SaveChanges();
                MessageBox.Show("Data Berhasil disimpan");            }
            catch (Exception ex)
            {
                MessageBox.Show("Data Gagal disimpan");
            }

            resetField();
            DisplayData();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            mh mhs = getByIdMhs(getIdMhsRowsValue());

            if (mhs != null)
            {
                mId.Text = mhs.IdMhs;
                mName.Text = mhs.Nama;
                mAlamat.Text = mhs.Alamat;
                mJurusan.Text = mhs.Jurusan;
                mJenisKelamin.Text = mhs.JenisKelamin;
            }
            else
            {
                MessageBox.Show("Data Tidak ditemukan dengan");
            }

        }

        private void btnHapus_Click(object sender, EventArgs e)
        {
            mh mhs = getByIdMhs( getIdMhsRowsValue() );

            DialogResult result = MessageBox.Show("Apakah anda yakin ?", 
                "Remove Mhs",
                MessageBoxButtons.OKCancel, 
                MessageBoxIcon.Asterisk);

            if( result.Equals(DialogResult.OK))
            {
                db.mhs.Remove(mhs);
                db.SaveChanges();
                DisplayData();
            }
        }

        public string getIdMhsRowsValue()
        {
            DataGridViewRow row = mGridView.CurrentRow;
            return row.Cells["IdMhs"].Value.ToString();
        }


        private mh getByIdMhs(string id)
        {
            mh mhs = db.mhs.Where(m => m.IdMhs.Equals(id)).FirstOrDefault();
            return mhs;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'db_mahasiswaDataSet.Buku' table. You can move, or remove it, as needed.
            this.bukuTableAdapter.Fill(this.db_mahasiswaDataSet.Buku);

        }

        private void bSimpan_Click(object sender, EventArgs e)
        {
            var nama = bName.Text.ToString();
            var pengarang = bPengarang.Text.ToString();
            var id = bId.Text.ToString();
            if( string.IsNullOrEmpty(nama) || string.IsNullOrEmpty(pengarang))
            {
                MessageBox.Show("Nama / Pengarang tidak boleh kosong");
            }
            else
            {
                Buku bookObj = null;
                if( !string.IsNullOrEmpty(id))
                {
                    int idBook = 0;
                    if (int.TryParse(id,out idBook))
                    {
                        bookObj = db.Bukus.Find(idBook);
                    }

                    if (bookObj == null) bookObj = new Buku();
                }
                else
                {
                    bookObj = new Buku();
                }     
                
                bookObj.Name = nama;
                bookObj.Pengarang = pengarang;
                if (bookObj.Id == 0)
                {
                    bookObj.Status = true;
                    db.Bukus.Add(bookObj);
                }
                else
                {
                    db.Entry(bookObj).State = System.Data.Entity.EntityState.Modified;
                }
                db.SaveChanges();
                DisplayDataBook();

                resetBookField();
            }
        }

        private void bUpdate_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = bGridView.CurrentRow;
            var id = row.Cells["Id"].Value.ToString();
            int idBook = 0;
            if( int.TryParse(id,out idBook)){
                var book = db.Bukus.Find(idBook);
                if( book == null)
                {
                    MessageBox.Show("Buku tidak ditemukan dengan ID = "+id);
                }
                else
                {
                    bId.Text = book.Id.ToString();
                    bName.Text = book.Name;
                    bPengarang.Text = book.Pengarang;
                }
            }
            DisplayDataBook();
        }

        private void bDelete_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = bGridView.CurrentRow;
            var id = row.Cells["Id"].Value.ToString();
            int idBook = 0;
            if (int.TryParse(id, out idBook))
            {
                var book = db.Bukus.Find(idBook);
                if (book == null)
                {
                    MessageBox.Show("Buku tidak ditemukan dengan ID = " + id);
                }
                else
                {
                    db.Bukus.Remove(book);
                    db.SaveChanges();
                }
            }
            DisplayDataBook();
        }

        private void pPinjam_Click(object sender, EventArgs e)
        {
            var user = ((mh)pIdUser.SelectedItem);
            var book = ((Buku)pIdBook.SelectedItem);

            if( book.Status == true)
            {
                var dateNow = DateTime.Now;
                PeminjamanBuku peminjaman = new PeminjamanBuku();
                peminjaman.IdMhs = user.IdMhs;
                peminjaman.IdBuku = book.Id;
                peminjaman.Pengambilan = dateNow;
                peminjaman.Pengembalian = dateNow.AddDays(1);
                db.PeminjamanBukus.Add(peminjaman);
                db.SaveChanges();

                Buku buku = db.Bukus.Find(peminjaman.IdBuku);
                if (buku != null)
                {
                    buku.Status = false;
                    db.Entry(buku).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    DisplayDataPeminjaman();
                }
                DisplayDataPeminjaman();
                MessageBox.Show("Buku Sukses Dipinjam");
            }
            else
            {
                PeminjamanBuku peminjaman = db.PeminjamanBukus.Where(m => m.IdBuku == book.Id
                && m.PengembalianUser == null).FirstOrDefault(); ;
                if (peminjaman != null)
                {
                    MessageBox.Show("Buku telah dipinjam oleh " + peminjaman.mh.Nama);
                }
            }
        }

        private void pKembalikan_Click(object sender, EventArgs e)
        {

            DataGridViewRow row = PGridView.CurrentRow;
            var id = row.Cells["Id"].Value.ToString();
            int idPeminjaman = 0;
            if (int.TryParse(id, out idPeminjaman))
            {
                PeminjamanBuku peminjaman = db.PeminjamanBukus.Find(idPeminjaman);
                if( peminjaman != null)
                {
                    peminjaman.PengembalianUser = DateTime.Now;
                    db.Entry(peminjaman).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    Buku buku = db.Bukus.Find(peminjaman.IdBuku);
                    if( buku != null)
                    {
                        buku.Status = true;
                        db.Entry(buku).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    DisplayDataPeminjaman();
                    MessageBox.Show("Buku Telah dikembalikan");
                }
                else
                {
                    MessageBox.Show("Peminjaman Id tidak ditemukan");
                }
            }
        }

        //private void pHapus_Click(object sender, EventArgs e)
        //{
        //    DataGridViewRow row = PGridView.CurrentRow;
        //    var id = row.Cells["Id"].Value.ToString();
        //    int idPeminjaman = 0;
        //    if (int.TryParse(id, out idPeminjaman))
        //    {
        //        PeminjamanBuku peminjaman = db.PeminjamanBukus.Find(idPeminjaman);
        //        db.PeminjamanBukus.Remove(peminjaman);
        //        db.SaveChanges();
        //        DisplayDataPeminjaman();

        //        Buku buku = db.Bukus.Find(peminjaman.IdBuku);
        //        if (buku != null)
        //        {
        //            buku.Status = true;
        //            db.Entry(buku).State = System.Data.Entity.EntityState.Modified;
        //            db.SaveChanges();
        //            DisplayDataPeminjaman();
        //        }
        //    }
        //}
    }
}
